import usocket
print('AF_UNIX', repr(usocket.AF_UNIX))
print('AF_INET', repr(usocket.AF_INET))
print('AF_INET6', repr(usocket.AF_INET6))
print()
print('SOCK_STREAM', repr(usocket.SOCK_STREAM))
print('SOCK_DGRAM', repr(usocket.SOCK_DGRAM))
print('SOCK_RAW', repr(usocket.SOCK_RAW))
# print('SOCK_RDM', repr(usocket.SOCK_RDM))
# print('SOCK_SEQPACKET', repr(usocket.SOCK_SEQPACKET))
# print()
# print('SOCK_CLOEXEC', repr(usocket.SOCK_CLOEXEC))
# print('SOCK_NONBLOCK', repr(usocket.SOCK_NONBLOCK))
# print()
# print('IPPROTO_TCP', repr(usocket.IPPROTO_TCP))
print()

# import uctypes # does not contain yet LoadLibrary
# TODO try https://pycopy.readthedocs.io/en/latest/library/ffi.html
# import sys
# _lib = uctypes.cdll.LoadLibrary(sys.argv[0])
# print(_lib.socket(socket.AF_INET, socket.SOCK_STREAM, socket.IPPROTO_TCP))

print(usocket.socket(usocket.AF_INET, usocket.SOCK_STREAM))
print(usocket.socket())

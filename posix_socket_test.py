import socket
print('AF_UNIX', repr(socket.AF_UNIX))
print('AF_INET', repr(socket.AF_INET))
print('AF_INET6', repr(socket.AF_INET6))
print()
print('SOCK_STREAM', repr(socket.SOCK_STREAM))
print('SOCK_DGRAM', repr(socket.SOCK_DGRAM))
print('SOCK_RAW', repr(socket.SOCK_RAW))
print('SOCK_RDM', repr(socket.SOCK_RDM))
print('SOCK_SEQPACKET', repr(socket.SOCK_SEQPACKET))
print()
print('SOCK_CLOEXEC', repr(socket.SOCK_CLOEXEC))
print('SOCK_NONBLOCK', repr(socket.SOCK_NONBLOCK))
print()
print('IPPROTO_TCP', repr(socket.IPPROTO_TCP))
print()

import argparse
parser = argparse.ArgumentParser()
parser.add_argument("libc")
args = parser.parse_args()
import ctypes
_lib = ctypes.cdll.LoadLibrary(args.libc)
print(_lib.socket(socket.AF_INET, socket.SOCK_STREAM, socket.IPPROTO_TCP))
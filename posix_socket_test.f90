program testsocket
  use, intrinsic ::  iso_c_binding

  implicit none

  INTEGER,PARAMETER  ::AF_INET = 2
  INTEGER,PARAMETER  ::SOCK_STREAM = 1
  INTEGER,PARAMETER  ::IPPROTO_TCP = 6

  interface
    function socket(domain, type, protocol) bind(c, name="socket")
      use, intrinsic :: iso_c_binding
      integer(kind=c_int) :: socket
      integer(kind=c_int), value :: domain, type, protocol
    end function socket
  end interface

  integer :: sock

  sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)

  write(*,*) "Socket returned: ", sock

end program testsocket